#pragma once
typedef unsigned long long u64;
typedef signed long long i64;
typedef unsigned long usize;
typedef signed long isize;
typedef unsigned int u32;
typedef signed int i32;
typedef unsigned short u16;
typedef signed short i16;
typedef unsigned char u8;
typedef signed char i8;

typedef unsigned char bool;
#define FALSE 0
#define TRUE 1

typedef u8 efi_guid[16];
typedef void* efi_handle;
typedef void* efi_event;
typedef usize efi_status;
typedef u64 efi_block_address;
typedef u64 efi_physical_address;
typedef u64 efi_virtual_address;
typedef usize efi_tpl;
typedef u8 mac_address[32];
typedef u8 ipv4_address[4];
typedef u8 ipv6_address[16];
typedef union {
    ipv4_address v4;
    ipv6_address v6;
} ip_address;

#define FN_PTR(name) efi_fn_ ## name name;
#define FN_DEF(ret, name, params...) typedef ret (* efi_fn_ ## name)(params); ret efi_impl_ ## name (params);