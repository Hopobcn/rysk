#pragma once
#include "types.h"

struct efi_simple_text_input_protocol;

struct efi_input_key {
    u16 scan_code;
    u16 unicode_char;
};

FN_DEF(efi_status, reset_input, struct efi_simple_text_input_protocol* this, bool extended_verification)
FN_DEF(efi_status, read_key_stroke, struct efi_simple_text_input_protocol* this, struct efi_input_key* key)

#define EFI_SIMPLE_TEXT_INPUT_PROTOCOL_GUID {0x38, 0x74, 0x77, 0xc1, 0x69, 0xc7, 0x11, 0xd2, 0x8e, 0x39, 0x00, 0xa0, 0xc9, 0x69, 0x72, 0x3b}
struct efi_simple_text_input_protocol {
    FN_PTR(reset_input)
    FN_PTR(read_key_stroke)
    efi_event wait_for_key;
};

struct efi_simple_text_output_protocol;
struct efi_simple_text_output_mode {
    i32 max_mode;
    i32 mode;
    i32 attribute;
    i32 cursor_column;
    i32 cursor_row;
    bool cursor_visible;
};

FN_DEF(efi_status, reset_output, struct efi_simple_text_output_protocol* this, const bool extended_verification)
FN_DEF(efi_status, output_string, struct efi_simple_text_output_protocol* this, const u16* string)
FN_DEF(efi_status, test_string, struct efi_simple_text_output_protocol* this, const u16* string)
FN_DEF(efi_status, query_output_mode, struct efi_simple_text_output_protocol* this, const usize mode_number, usize* columns, usize* rows)
FN_DEF(efi_status, set_output_mode, struct efi_simple_text_output_protocol* this, const usize mode_number)
FN_DEF(efi_status, set_output_attribute, struct efi_simple_text_output_protocol* this, const usize attribute)
FN_DEF(efi_status, clear_screen, struct efi_simple_text_output_protocol* this)
FN_DEF(efi_status, set_cursor_position, struct efi_simple_text_output_protocol* this, const usize column, const usize row)
FN_DEF(efi_status, enable_cursor, struct efi_simple_text_output_protocol* this, const bool visible)

struct efi_simple_text_output_protocol {
    FN_PTR(reset_output)
    FN_PTR(output_string)
    FN_PTR(test_string)
    FN_PTR(query_output_mode)
    FN_PTR(set_output_mode)
    FN_PTR(set_output_attribute)
    FN_PTR(clear_screen)
    FN_PTR(set_cursor_position)
    FN_PTR(enable_cursor)
    struct efi_simple_text_output_mode* mode;
};