#pragma once
#include "types.h"
#include "protocol/path.h"

struct efi_load_file_protocol;
FN_DEF(efi_status, load_file, struct efi_load_file_protocol* this, struct efi_device_path_protocol* file_path, bool boot_policy, usize* buffer_size, void* buffer)
struct efi_load_file_protocol {
    FN_PTR(load_file)
};