#pragma once
#include "types.h"
#include "table.h"
#include "memory.h"
#include "protocol/path.h"

struct efi_system_table;

typedef enum {
    TimerCancel,
    TimerPeriodic,
    TimerRelative
} efi_timer_delay;

FN_DEF(void, event_notify, const efi_event event, const void* context)
FN_DEF(efi_status, create_event, const u32 type, const efi_tpl notify_tpl, const efi_fn_event_notify notify_function, const void* notify_context, efi_event* event)
FN_DEF(efi_status, create_event_ex, const u32 type, const efi_tpl notify_tpl, const efi_fn_event_notify notify_function, const void* notify_context, const efi_guid* event_group, efi_event* event)
FN_DEF(efi_status, close_event, const efi_event event)
FN_DEF(efi_status, signal_event, const efi_event event)
FN_DEF(efi_status, wait_for_event, const usize number_of_events, const efi_event* event, usize* index)
FN_DEF(efi_status, check_event, const efi_event event)
FN_DEF(efi_status, set_timer, const efi_event event, const efi_timer_delay type, const u64 trigger_time)
FN_DEF(efi_tpl, raise_tpl, const efi_tpl new_tpl)
FN_DEF(void, restore_tpl, const efi_tpl old_tpl)

enum efi_allocate_type {
    AllocateAnyPages,
    AllocateMaxAddress,
    AllocateAddress,
    MaxAllocateType
};
enum efi_memory_type {
    EfiReservedMemoryType,
    EfiLoaderCode,
    EfiLoaderData,
    EfiBootServicesCode,
    EfiBootServicesData,
    EfiRuntimeServicesCode,
    EfiRuntimeServicesData,
    EfiConventionalMemory,
    EfiUnusableMemory,
    EfiACPIReclaimMemory,
    EfiACPIMemoryNVS,
    EfiMemoryMappedIO,
    EfiMemoryMappedIOPortSpace,
    EfiPalCode,
    EfiPersistentMemory,
    EfiMaxMemoryType
};

FN_DEF(efi_status, allocate_pages, const enum efi_allocate_type type, const enum efi_memory_type memory_type, const usize pages, efi_physical_address* memory)
FN_DEF(efi_status, free_pages, const efi_physical_address memory, const usize pages)
FN_DEF(efi_status, get_memory_map, usize* memory_map_size, struct efi_memory_descriptor* memory_map, usize* map_key, usize* descriptor_size, u32* descriptor_version)
FN_DEF(efi_status, allocate_pool, const enum efi_memory_type pool_type, const usize size, void** buffer)
FN_DEF(efi_status, free_pool, const void* buffer)

enum efi_interface_type {
    EfiNativeInterface
};
enum efi_locate_search_type {
    AllHandles,
    ByRegisterNotify,
    ByProtocol
};
struct efi_open_protocol_information_entry {
    void* agent_handle;
    void* controller_handle;
    u32 attributes;
    u32 open_count;
};


FN_DEF(efi_status, install_protocol_interface, efi_handle handle, const efi_guid* protocol, const enum efi_interface_type interface_type, const void* interface)
FN_DEF(efi_status, uninstall_protocol_interface, const efi_handle handle, const efi_guid* protocol, const void* interface)
FN_DEF(efi_status, reinstall_protocol_interface, const efi_handle handle, const efi_guid* protocol, const void* old_interface, const void* new_interface)
FN_DEF(efi_status, register_protocol_notify, const efi_guid* protocol, const efi_event event, void** registration)
FN_DEF(efi_status, locate_handle, const enum efi_locate_search_type search_type, const efi_guid* protocol, const void* search_key, usize* buffer_size, void* buffer)
FN_DEF(efi_status, handle_protocol, const efi_handle handle, const efi_guid* protocol, void** interface)
FN_DEF(efi_status, locate_device_path, const efi_guid* protocol, struct efi_device_path_protocol** device_path, void* device)
FN_DEF(efi_status, open_protocol, const efi_handle handle, const efi_guid* protocol, void** interface, const void* agent_handle, const void* controller_handle, const u32 attributes)
FN_DEF(efi_status, close_protocol, const efi_handle handle, const efi_guid* protocol, const void* agent_handle, const void* controller_handle)
FN_DEF(efi_status, open_protocol_information, const efi_handle handle, const efi_guid* protocol, struct efi_open_protocol_information_entry** entry_buffer, usize* entry_count)
FN_DEF(efi_status, connect_controller, const efi_handle controller_handle, const void** driver_image_handle, const struct efi_device_path_protocol* remaining_device_path, const bool recursive)
FN_DEF(efi_status, disconnect_controller, const efi_handle controller_handle, const void* driver_image_handle, const void* child_handle)
FN_DEF(efi_status, protocols_per_handle, const efi_handle handle, efi_guid*** protocol_buffer, usize* protocol_buffer_count)
FN_DEF(efi_status, locate_handle_buffer, const enum efi_locate_search_type search_type, const efi_guid* protocol, const void* search_key, usize* number_handles, void*** buffer)
FN_DEF(efi_status, locate_protocol, const efi_guid* protocol, const void* registration, void** interface)
FN_DEF(efi_status, install_multiple_protocol_interfaces, efi_handle* handle, ...)
FN_DEF(efi_status, uninstall_multiple_protocol_interfaces, const efi_handle* handle, ...)

typedef efi_status (*image_entry_point)(const void* image_handle, const struct efi_system_table* system_table);

FN_DEF(efi_status, load_image, const bool boot_policy, const void* parent_image_handle, const struct efi_device_path_protocol* device_path, const void* source_buffer, const usize source_size, void** image_handle)
FN_DEF(efi_status, start_image, const void* image_handle, usize* exit_data_size, u16** exit_data)
FN_DEF(efi_status, unload_image, const void* image_handle)
FN_DEF(efi_status, exit, const void* image_handle, const efi_status exit_status, const usize exit_data_size, const u16* exit_data)
FN_DEF(efi_status, exit_boot_services, const void* image_handle, const usize map_key)

FN_DEF(efi_status, set_watchdog_timer, const usize timeout, const u64 watchdog_code, const usize data_size, const u16* watchdog_data)
FN_DEF(efi_status, stall, const usize microseconds)
FN_DEF(void, copy_mem, void* destination, void* source, const usize length)
FN_DEF(void, set_mem, void* buffer, const usize size, const u8 value)
FN_DEF(efi_status, get_next_monotonic_count, u64* count)
FN_DEF(efi_status, install_configuration_table, const efi_guid* guid, void* table)
FN_DEF(efi_status, calculate_crc32, const void* data, const usize data_size, u32* crc32)



#define EFI_BOOT_SERVICES_SIGNATURE 0x56524553544f4f42
struct efi_boot_services_table {
    struct efi_table_header header;

    FN_PTR(raise_tpl)
    FN_PTR(restore_tpl)

    FN_PTR(allocate_pages)
    FN_PTR(free_pages)
    FN_PTR(get_memory_map)
    FN_PTR(allocate_pool)
    FN_PTR(free_pool)

    FN_PTR(create_event)
    FN_PTR(set_timer)
    FN_PTR(wait_for_event)
    FN_PTR(signal_event)
    FN_PTR(close_event)
    FN_PTR(check_event)

    FN_PTR(install_protocol_interface)
    FN_PTR(reinstall_protocol_interface)
    FN_PTR(uninstall_protocol_interface)
    FN_PTR(handle_protocol)
    void* _reserved;
    FN_PTR(register_protocol_notify)
    FN_PTR(locate_handle)
    FN_PTR(locate_device_path)
    FN_PTR(install_configuration_table)

    FN_PTR(load_image)
    FN_PTR(start_image)
    FN_PTR(exit)
    FN_PTR(unload_image)
    FN_PTR(exit_boot_services)

    FN_PTR(get_next_monotonic_count)
    FN_PTR(stall)
    FN_PTR(set_watchdog_timer)

    FN_PTR(connect_controller)
    FN_PTR(disconnect_controller)

    FN_PTR(open_protocol)
    FN_PTR(close_protocol)
    FN_PTR(open_protocol_information)

    FN_PTR(protocols_per_handle)
    FN_PTR(locate_handle_buffer)
    FN_PTR(locate_protocol)
    FN_PTR(install_multiple_protocol_interfaces)
    FN_PTR(uninstall_multiple_protocol_interfaces)

    FN_PTR(calculate_crc32)

    FN_PTR(copy_mem)
    FN_PTR(set_mem)
    FN_PTR(create_event_ex)
};