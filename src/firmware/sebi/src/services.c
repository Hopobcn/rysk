#include "services.h"

const struct sebi_services sebi_services = {
    .shutdown = sebi_service_shutdown,
    .log = sebi_service_log,
};

__attribute__((noreturn)) void sebi_service_shutdown() {
    *((volatile u8*) 0) = 1;
    __builtin_unreachable();
}

void sebi_service_log(char* to_output, usize length) {
    for (; length > 0; to_output++, length--)
        *((volatile u8*) 2) = *to_output;
}