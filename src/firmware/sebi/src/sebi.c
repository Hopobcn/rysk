#include "types.h"
#include "sebi.h"
#include "elf.h"

const struct sebi_systab sebi_system_table = {
    .services = &sebi_services,
    .version = 0,
};

// 255 bytes of stack is plenty
#define SYSTEM_STACK_START 0x8000FF

#define APPLICATION_STACK_START 0xFF00000

// Initialise the stack
__attribute__((naked,noreturn)) void sebi(void) {
    asm volatile(
        "li sp,%0\n"
        "call load\n"
        "li a1,1\n"
        "sb a1,(zero)\n"
        ::"i"(SYSTEM_STACK_START)
    );
}

__attribute__((noreturn)) void panic(char* msg, usize length) {
    sebi_service_log(msg, length);
    sebi_service_shutdown();
    __builtin_unreachable();
}

bool streq(const char*, const char*);

void exec(sebi_entry);

#define ELF_FILE_ADDRESS 0x10000
void load(void) {
    // An ELF file should be at the start of the memory-mapped flash memory
    ElfHeader* header = (ElfHeader*)ELF_FILE_ADDRESS;

    if (
        header->identifier[0] != 0x7F
            || header->identifier[1] != 'E'
            || header->identifier[2] != 'L'
            || header->identifier[3] != 'F'
            || header->identifier[4] == ELF_CLASS_INVALID
            || header->identifier[5] != ELF_LAYOUT_LSB
            || header->identifier[6] == ELF_VERSION_INVALID
            || header->section_header_size != sizeof(ElfSectionHeader)
    ) panic(LSTR("Invalid ELF file at the start of flash memory"));

    bool elf32 = header->identifier[4] == ELF_CLASS_32BIT;

    ElfSectionHeader* sections = (ElfSectionHeader*)(ELF_FILE_ADDRESS + header->section_header_table);
    usize section_count = header->section_header_count;
    usize section_header_size = header->section_header_size;

    ElfSectionHeader* string_table = &sections[header->string_table_index];
    char* string_table_data = (char*)(ELF_FILE_ADDRESS + string_table->offset);

    for (usize index = 1; index < section_count; index++) {
        ElfSectionHeader* section = &sections[index];
        char* name = &string_table_data[section->name];
        // For now just copy text and rodata sections instead of being flexible
        if (streq(name, ".text") || streq(name, ".rodata")) {
            u8* section_address = (u8*)section->address;
            usize size = section->size;
            u8* section_data = (u8*)(ELF_FILE_ADDRESS + section->offset);
            for (; size > 0; size--, section_address++, section_data++)
                *((u8*)(section_address)) = *section_data;
        }
    }

    if (header->entry == 0)
        panic(LSTR("Entry point for elf file is null\n"));
    exec((sebi_entry)(header->entry));
}

void exec(sebi_entry entry) {
    u32 code;
    asm(
        // Move the stack to the end of usable memory 
        "li a0,%0\n"
        // Store our stack pointer just above the new stack
        "sw sp,4(a0)\n"
        "mv sp,a0"
        ::"i"(APPLICATION_STACK_START - 4):"a0"
    );
    asm(
        // First parameter is a pointer to the sebi_systab
        "mv a0,%2\n"
        // Call the entry function of the loaded application
        "jalr %1\n"
        // Restore the stack pointer
        "lw sp,4(sp)\n"
        // Get the return value
        "mv %0,a0"
        : "=r"(code) : "r"(entry), "r"(&sebi_system_table) : "a0"
    );

    if (!code)
        sebi_service_log(LSTR("Exit code: SUCCESS\n"));
    else
        sebi_service_log(LSTR("Exit code: ERROR\n"));
}

inline void memset(void* address, usize bytes, u8 value) {
    for (usize index = 0; index < bytes; index++)
        *((u8*)address + index) = value;
}

/// Compare 2 strings, returning if they are equal up to, but not including, the first null terminator
bool streq(const char* address1, const char* address2) {
    for (; *address1 != 0 && *address2 != 0; address1++, address2++)
        if (*address1 != *address2)
            return false;
    return true;
}

// RV32I does not provide a multiplication operation so a software one must be used
int __mulsi3(int left, int right) {
    // Naive implementation for now
    bool is_signed = (right & 0x80000000) != 0;
    if (is_signed) {
        // Flip the sign on both if right is signed
        right = (right ^ -1) + 1;
        left = (right ^ -1) + 1;
    }
    volatile int out = 0;
    for (; right > 0; right--)
        out += left;
    return out;
}